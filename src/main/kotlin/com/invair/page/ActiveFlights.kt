package com.invair.page

import com.invair.domain.Flight
import com.invair.domain.Pilot
import com.invair.main
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.How
import org.openqa.selenium.support.PageFactory

class ActiveFlights (private val webDriver: WebDriver) {

    init {
        PageFactory.initElements(webDriver, this)
    }

    @FindBy(how = How.CLASS_NAME, using = "fsa_table")
    private val mainTable: WebElement? = null

    fun getFlights(): List<Flight> {
        val flights = mutableListOf<Flight>()
        val rows = mainTable?.findElements(By.tagName("TR"))
        for(row in rows.orEmpty()){
            val columns = row.findElements(By.tagName("TD"))
            if(columns.isNotEmpty()) {
                val pilot = columns[0].text.split(" - ")
                val aircraft = columns[1].text
                val depArr = columns[2].text.split("-")
                val pax = toInt(columns[3].text)
                val cargo = toInt(columns[4].text)
                val status = columns[5].text
                val flight = Flight(Pilot(pilot[0], pilot[1]), aircraft, depArr[0], depArr[1], pax, cargo, status)
                flights.add(flight)
            }
        }
        return flights
    }

    private fun toInt(str: String): Int{
        return str.replace(",","").toInt()
    }
}