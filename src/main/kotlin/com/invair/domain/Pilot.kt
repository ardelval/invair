package com.invair.domain

data class Pilot (val id: String, val name: String)