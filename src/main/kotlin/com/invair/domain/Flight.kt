package com.invair.domain

data class Flight (val pilot: Pilot,
                   val aircraft: String,
                   val departure: String,
                   val arrival: String,
                   val pax: Int,
                   val cargo: Int,
                   val status: String,
                   val distance: String,
                   val date: String,
                   val rating: Int){
    constructor(pilot: Pilot, aircraft: String, departure: String, arrival: String):
        this(pilot, aircraft, departure, arrival, 0, 0, "")

    constructor(pilot: Pilot, aircraft: String, departure: String, arrival: String, pax: Int, cargo: Int, status: String):
            this(pilot, aircraft, departure, arrival, pax, cargo, status, "", "", 0)
}


