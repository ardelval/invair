package com.invair.logic

import com.invair.domain.Flight
import com.invair.page.ActiveFlights
import org.openqa.selenium.WebDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.PageFactory

class FlightUseCase {

    val driver = HtmlUnitDriver()

    fun getActiveFlights() : List<Flight>{
        driver.get("http://remote.fsairlines.net/v1/flights_active.php?rvi=44128")
        val activeFlights = ActiveFlights(driver)
        return activeFlights.getFlights()
    }
}