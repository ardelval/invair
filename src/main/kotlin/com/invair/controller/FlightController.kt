package com.invair.controller

import com.invair.domain.Flight
import com.invair.logic.FlightUseCase
import org.springframework.http.MediaType.*
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("flight")
class FlightController {

    private val flightUseCase = FlightUseCase()

    @PostMapping(
            value = ["/active"],
            produces = [APPLICATION_JSON_VALUE]
    )
    fun active(): List<Flight> {
        return flightUseCase.getActiveFlights()
    }
}